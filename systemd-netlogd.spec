Name:           systemd-netlogd
Version:        1.2
Release:        1%{?dist}
Summary:        Forwards messages from the journal to other hosts over the network using syslog format RFC 5424

License:        GPLv2 and LGPL-2.1+ and CC0
URL:            https://github.com/systemd/systemd-netlogd
Source0:        %{URL}/archive/refs/tags/v1.2.tar.gz

BuildRequires:  gperf
BuildRequires:  libcap-devel
BuildRequires:  pkgconfig
BuildRequires:  systemd-devel
BuildRequires:  python3-sphinx
BuildRequires:  meson >= 0.43
BuildRequires:  gcc

%description
Forwards messages from the journal to other hosts over the network using
the Syslog Protocol (RFC 5424). It can be configured to send messages to
both unicast and multicast addresses. systemd-netlogd runs with own user
systemd-journal-netlog. Starts sending logs when network is up and stops
sending as soon as network is down (uses sd-network).

%prep
%setup -q -n %{name}-%{version}
rm -rf build && mkdir build

%build
pushd build
  meson ..
  ninja-build -v
popd

%install
pushd build
  DESTDIR=%{buildroot} ninja-build -v install
popd

%post
useradd -r -d / -s /usr/sbin/nologin -g systemd-journal systemd-journal-netlog || :

%files
%{_sysconfdir}/systemd/systemd-netlogd.conf
/lib/systemd/system/systemd-netlogd.service
/lib/systemd/systemd-netlogd
%{_mandir}/man1/systemd-netlogd.1*

%changelog
* Mon Oct 31 2022 Marco Hartgring <marco.hartgring@gmail.com> - 1.2-1
- Updated to 1.2
* Fri Oct 04 2019 Recteur LP <recteurlp@gmail.com> - 1.1-2
- Change config and service file path
* Mon May 21 2018 Susant Sahani <susant@redhat.com> - 1.1-1
- Initial spec
